//let use the require directive in order access the jsonwebtoken library
const jwt = require('jsonwebtoken'); 

//lets declare a secret word
const secret = 'kahitanongsecretWord'; 


//Lets create a function that will allow us to generate and produce an access token.
  //Pack the gift and provide a lock with a secret code as the key in order to unlock the package.
module.exports.createAccessToken = (user) => {
	
	const userInfo = {
			//this object will hold the information about the user who wants to access his account.

			//we will extract the ff info about the user.
			id: user._id, //to check if the user exists in the collection
			email: user.email, //to check if the user matches the info from the login
			isAdmin: user.isAdmin //limitation of permission.
		}; 

	//this will be used to authorize a user. 
	//Lets generate an access token using the jwt's sign method

		//sign() will be used to create a virtual signature that contains the information about the system and the verified user. this is a built-in function in the jwt dependency.
	return jwt.sign(userInfo, secret);
       
}; 

//Lets create a function that will "verify" the information inside the JWT. 
  //Receiving the gift and using the lock to open and to verify if the package came from the legitimate sender and to make sure if the package has not been tampered with.  
module.exports.verify = (req, res, next) => {
	//upon verifying a user's request we will receive the JWT.
	//you need to get the value of the access token
	//we need to capture the authorization key in the headers section of the request.
	  //The token is retrieved from the request header
	  //This can provided in the Postman client app under
	      //Authorization > Bearer Token
	let token = req.headers.authorization;

	//lets create a control structure if the access token is received or NOT. we need to verify if the access token is JWT or undefined.
	if (typeof token !== "undefined") {
		//create a checker to print the value of the access token
		// console.log(token); //Bearer token
		// console.log(typeof token); //string
       
		//we need to get the "exact" value of the token 
		let newToken = token.slice(7, token.length);
		console.log(newToken); 

		//our next step now is to validate the token by using the verify() method of the JWT library, this method will allow us to decrypt the token using the secret code of the app 

		//the secret word will serve as the "key" to unlock the contents of the token.
		// verify(lock, key, actions/procedures that will upon successfully unlocking the token)
		return jwt.verify(newToken, secret, (err, payload) => {
			//if JWT is NOT VALID
			if (err) {
				return res.send({ status: "JWT failed to verify"});
			} else {
                //if JWT is valid 
                //call the argument "next"
                //this allows the application to proceed with the next middleware/function/callback inside the route/.
                next();
			}
		})
	} else {
        //if the Token does NOT EXISTS.
        res.send({ auth: "failed"});
	} 
}

  //Open the gift and get it's contents to use. 
module.exports.decode = (token) => {
   //we need to get the exact value of the access token
   //'Bearer ' + token
   if (typeof token !== "undefined") {
      //valid si JWT

      //slice() => this method extracts parts of a string and returns the extracted section in a new string.

      //SYNTAX: slice(start, end)
      //it will start extracting from the 7th position and it will terminate at the last character of the string.
      token = token.slice(7, token.length);

      //we need to verify the value of the jwt
      return jwt.verify(token, secret, (err, payload) => {

      	 //lets create a control structure that will determine the response if the payload is extracted or not.
      	 if (err) {
      	 	//this will be the return if the message is unrecognized or untrusted.
      	 	return null; 
      	 } else {
      	 	//if the JWT is valid, its now time to decode and extract the values of the payload so that we can get the "claims".
      	 		//how are we going to "decode" the accessToken?
      	 		//"payload" => contains the information provided in the access token
      	 	//the object {} inserted inside the 2nd parameter of the function identifies the structue of the return of the method.
      	 		//why? (json, completed) pag ka naka json ang object ..siya naka stringify kaya hindi mapaparse or maaccess yung kanyang propeties.
      	 	return jwt.decode(token, { complete: true }).payload;  
      	 	//the "decode" method is ysed to obtain the information inside the JWT.
      	 	//the docode() will return an object with access to the decoded structure of the token key.
      	 	  //SYNTAX: libraryName.decode(token, [options]) 
      	 	  		//token => is the json web token string
      	 	  		//options: we will pass down an object that will return the decoded payload and header.
      	 	  		  //complete => this will return an object with the decoded payload and header. 
      	 	  		  //{ complete: true } => this option allows us to return additional information from the JWT key.
      	 }

      })

   } else {
   	  //Token does not exists
   	  return null; 
   }
}


//WHY DID I DECIDED TO VERIFY THE TOKEN FIRST BEFORE I DECODED THE MESSAGE?

 //==> The decode() function WILL NOT VERIFY WHETHER THE SIGNATURE IS VALID. IN THE DOCUMENTATION OF THE JWT LIBARY, Executing the decode() COMES WITH A WARNING. Developers should NOT EXECUTE, or USE the method for UNTRUSTED MESSAGES. 
