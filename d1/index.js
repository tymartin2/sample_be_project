const express = require('express');
const mongoose = require('mongoose'); //acquire the necessary library to connect to our DBMS

	//lets add the setup to connect to the database.
	//the connection string holds the information where the data will be stores as well as the information about the database user.
const courseRoutes = require('./routes/course.js');
//define the routes for our API collection for the users.
const userRoutes = require('./routes/user.js');

mongoose.connect('mongodb+srv://admin:vkrImtRnofl75Q4G@cluster0.qyycl.mongodb.net/b122_bookingapp?retryWrites=true&w=majority'); 

//set the notifications for connection issues(success/fail)
const db = mongoose.connection; //the connection property describes the connection of the app to the database


	//if the connection will fail (if an error occurred). we are going to use an event reference in JS. 
	//Upon connecting to the database an Event will be fired to. we are going to use JS Events to notify code for "interesting changes" that may take effect upon code execution.
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Were connected to the Cloud Database")); 


const app = express(); 
    
const port = 4000; 

//server setup/configuration 
app.use(express.json()); 
app.use(express.urlencoded({extended:true})); 

//lets define the route for our API methods for courses.
app.use("/courses", courseRoutes);
app.use("/users", userRoutes);

//visualization:
// http:localhost:4000/courses/create

//required? 
//is it necessary to store all related requests?
  
app.listen(port, () => console.log(`Server is running on ${port}!`)
); 
