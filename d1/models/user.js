const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
   	 firstName : {
   	    type: String,
   	    required: [true, "First Name is Required"]   	
   	 },
   	 middleName : {
   	    type: String, 
   	    required: [true, "Middle Name is Required"]   	
   	 },
   	 lastName : {
   	    type: String, 
   	    required: [true, "Last Name is Required"]   	
   	 },
   	 email : {
   	    type: String,
   	    required: [true, "Email is Required"]    
   	 }, 
   	 password : {
   	    type: String, 
   	    required: [true, "Password is Required"]   
   	 },
   	 mobileNo : {
   	    type: String,
   	    required: [true, "Mobile Number is Required"]   	
   	 },
   	 gender : {
   	    type: String,
   	    required: [true, "Gender is Required"]  	
   	 }, 
   	 enrollments : [
   	 		{
   	   		 courseId: {
   	   		 	type: String,
   	   		 	required: [true, "course ID is Required"]
   	   		 },
   	   		 enrolledOn: {   
   	   		 	type: Date,
   	   		 	default: new Date()
   	   		 },  
   	   		 status: {
   	   		 	type: String, 
   	   		 	default: "Regular"
   	   		 }	
   	 	}
   	 ],
   	 isAdmin : {
   	    type: Boolean,
   	    default: false  	
   	 } 
}); 

module.exports = mongoose.model("User", userSchema); 
//users ==> collection name
  
   

