const express = require('express'); 
const router = express.Router(); //the router constructor will allow us to assign a new route within our server side app.
const courseController = require('../controllers/course.js');
const auth = require('../auth.js');

//[Admin Restrictions]
  //we want to verify the user first.
  //determine wether the user is an admin or NOT. 
      //isAdmin === true  => admin
      //        === false => non admin / client
router.post('/create', auth.verify, (req, res) => {
	//get the request body.
	//i need to get the info about the user.
	//use the decode method to extract the information about the user. 
	//we only need to get the isAdmin key to be able to describe the permission to be granted upon the user. 
	const info = {
		course: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin 
	}
	courseController.addCourse(info).then(resultofAddCourse => res.send(resultofAddCourse));
}); 

router.get('/', (req, res) => {
   courseController.getAllCourses().then(resultOfQuery => res.send(resultOfQuery));
});

//we will now create a route using the /:parameterName, this will allow us to create a dynamic route. meaning the url/path is not static and the value changes depending on the information provided in the url.

//to retrieve all active courses.
router.get('/available', (req, res) => {
    courseController.getAllActive().then(resultFromFunction => res.send(resultFromFunction));
}); 
//make sure to separate static and dynamic routes.

//[DYNAMIC ROUTES]
router.get('/:targetId', (req, res) => {
	//before you execute this query make sure to acquire the value of the id inserted inside of the url params in the url path.
	console.log(req.params.targetId);  //checker that will check the value of the course ID.
	let courseId = req.params.targetId;
	courseController.getCourse(courseId).then(result => res.send(result)); 
}); 


// Create, Retrieve, "Update"
//should we create a static or "dynamic"?
router.put('/:targetId', (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});

//Route to archive/soft delete a course
//A "PUT" request is used instead of DELETE because of our approach in archiving and hiding the courses from our users thru the use of soft delete.
//lets create a new Dynamic route for our archive method.
//put(location/address, procedure)
router.put('/:courseId/archive', (req, res) => {
   courseController.archiveCourse(req.params).then(resultFromFunction => res.send(resultFromFunction));
})



module.exports = router; 