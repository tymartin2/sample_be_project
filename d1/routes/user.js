const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.js');
//lets acquire the auth module. 
const auth = require('../auth.js');

//[Static Routes]
router.post('/register', (req, response) => {
	//from our request we need to attach the body section that holds the details about the user that wants to create an account
	userController.registerUser(req.body).then(registerResults => {
		response.send(registerResults)
	}); 
});

//Route for user authentication
router.post('/login', (req, res) => {
	//you need to pass down the data that will be authenticated.
  userController.loginUser(req.body).then(result => res.send(result));
}); 

//Create a static Route to get user info
router.get('/details', auth.verify, (req, res) => {
  //How are we going to identify which user to target?
  //we will use the information encrypted in the access token of the request.

  //we accessed the authorization property of the headers of the request to be able to have access to the value of the token.

     //from the access token we need to extract the "claims" from the payload. this is not decoded yet. 
     //we need to decode in order to unlock the encrypted values inside the token
  const payload = auth.decode(req.headers.authorization);
  	//this "userData" na variable describe the payload body of the access token.
  userController.getProfile({ userId: payload.id}).then(result => res.send(result));
});












//[Dynamic Routes]
//This Route is to check for duplicate emails.
  //this route will be used to invoke the checkEmailExists() inside the controller.

   //base from your answers we will create a "dynamic route"
   //so far this endpoint is NOT YET RECOGNIZED BY THE APP.
router.get('/exists/:email', (req, res) => {

	//how to check if we have successfully attached an email addres in the url parameters of the request?
	console.log(req.params.email);

	userController.checkEmailExists(req.params).then(result => res.send(result));
});



module.exports = router; 