const Course = require('../models/Course.js');

//we are going to create a function that will allow us to create a new course inside the database.
  //you need to get the details/info about the new course that you wish to create

  //the data parameter we are referencing will come from the "request" made by the user
module.exports.addCourse = (data) => {
  //the "data" received is an object structure
  //we only want an admin user to be able to create a new course
  //lets created a control structure that will evaluate the value of the isAdmin property that was extracted from the access token. 
  if (data.isAdmin) {
    //procedure to happen
    //the user is an admin
      let newCourse = new Course({
            name: data.course.name, 
            description: data.course.description,
            price: data.course.price
      }); 
      return newCourse.save().then((course, error) => {
      
        if (error) {
          return false;
        } else {
                return true; 
        }
      })
  } else {
    return "you are NOT an Admin"
  };
};



module.exports.getAllCourses = () => {
	//you need to identify the collection where it's going to execute the query.
   return Course.find({}).then(outcome => {
   	   return outcome;
   });
}  

//lets create a function that will allow us to retrieve a document through its id property.
  //thru the use of this function we can simply target a specific document via the id value.
  //keep in mind that we are going to extract the value of the ID of the user inside the request url paramaters.
module.exports.getCourse = (urlReqParams) => {
  return Course.findById(urlReqParams).then(result => {
  	 return result;
  });  
}

//to create a function that will retrieve the documents that have an isActive status of "true" only. 

  //isActive => describe if a subject is still available for enrollment. 
  //true => available 
  //false => unavailable

module.exports.getAllActive = () => {
  //i need to identify a query that i will use in order to execute this task. 
    //how can you tell the query to search for a specific parameter? 
    //the value beingg passed inside the query will serve as the reference to determin what type of document it will look for. 
    //A Query also has a then() function. and thus can be used as a promise
  return Course.find({isActive: true}).then(result => {
    return result; 
  }); 
  //visualization courses.find() 
}


//lets create a function that will update a course inside the collection.

  //Steps: 
  //1. CREATE a variable which will contain the information about the changes that you want to apply to a document.

//lets identify 2 parameters in its argument section. which will the information that we will pass in our query.
module.exports.updateCourse = (requestUrlParameters, requestBody) => {
  //it needs to find the document first. 
  //update the properties.
  //is there a query in mongoose that will allow us to execute the 2 task above?

  //specify the fields of the document that we want to modify.
  let updates = {
    name: requestBody.name, 
    description: requestBody.description,
    price: requestBody.price
  }
  //syntax: findByIdAndUpdate(document ID, updates to be made)
  return Course.findByIdAndUpdate(requestUrlParameters.targetId,
    updates).then((courseUpdated, error) => {
      if (error) {
        return false; //course NOT UPDATED
      } else {
        return courseUpdated;  //SUCCESSFUL!!!!
      } //what if i want to get the new structure of the document as the response of the server.
      //will there be any restriction is a data was returned as a string? 
      //take note : YOU HAVE THE FREEDOM to create and identify the response that you want to receive BUT you have to be certain that you are receuving the correct data type if you want to parse/process the data.
  });
};

//true => false  //ALL GOOD!
//false => true(STRETCH GOAL)
//Group Task:
//CREATE an archive course function that will change the isActive property of the targeted course from "true" to "false"
//STRETCH GOAL: if the ID does NOT EXIST return a string message of "Course Not Found.". 

//function/controller name: archiveCourse. 
//make it a dynamic route /:targetId/archive
//45 mins to produce an output.

//if we archive a certain document we are implementing a "soft delete" management procedure on a document.

//=> soft delete => you are simply changing the  visibility level of a document to a client.
module.exports.archiveCourse = (request, response) => {
   //lets declare an object that will hold the section of the document that we would like to change. 
   let documentUpdates = {
     isActive: false
   }
   // findByIdAndUpdate(documentTarget, updatesToBeApplied);
   return Course.findByIdAndUpdate(request.courseId, documentUpdates).then((newCourse, error) => {
      //this method will be different from the approach that i did earlier.
      if (newCourse) {
        return Course.findById(newCourse._id).then(result => {
          return result; 
        }); 
      } else {
        return false; //scoping
      } //lets include an expression that will catch an error if the document was not found.

      //what if you want to display the object with the new changes back to the client?      
   }); 
}

  




