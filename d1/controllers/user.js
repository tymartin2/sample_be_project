const User = require('../models/User.js');
const bcrypt = require('bcrypt');
//lets acquire our auth.js document for us to be able to use its functionalities.
const auth = require('../auth.js')

module.exports.checkEmailExists = (requestParams) => {
	return User.find({ email: requestParams.email}).then(searchResult => {
		//we will identify the procedures that will upon receiving the result of the query.

		//this parameter "searchResult" => describes the record/s that will match the criteria given in the find()

		///the "find" method retuns a record/s if matches are found in the collection
		if (searchResult.length > 0) {
			return true; //e match has been found.
		} else {
			return "email is still available"; //NO DUPLICATES
		}
	}).catch((err) => {
		return "not found"; 
	})
	// users.find()
}

//lets create a function that will simulate a login request.
module.exports.loginUser = (body) => {
  //we are currently in the "authentication" stage of our login"
      return User.findOne({ email: body.email }).then(result => {
         if (!result) {
          //im going to replace the response to a boolean data type.
          //why? so that i will be able to easily evaluate the result. 
          //if a user does NOT EXISTS in the records.
            return false; 
         } else {
          //user EXISTS
          //now that we proven that the user exists it's time to authenticate the password. 
            //1. the password info in the client's API request. 
            //2. the encrypted password in the records found in the database for our user.

         //lets repackage the result of the function below
         //TIP: A good coding practice for carrying, describing a boolean value is to use the word 'is' or 'are' at the beginning, in the form of 'is + Noun'
           //example: isSingle, isDone, isreAdmin, arePeople.
            const isPasswordCorrect = bcrypt.compareSync(body.password, result.password); 
            //we will use the result of the compareSync() in order to determin the response to be sent out to the client.

            //if the passwords matched or if the result above is true
            if (isPasswordCorrect) {
              //lets generate an access token and place it inside an object structure/
              //we want to create a container for the access token so that the client will be able to identify what the encrypted data means.
              let signature = auth.createAccessToken(result);
              //console.log(signature); //we just did this as a checker
              return {
                accessToken: signature
              }; //for storage and labeling purposes.
            } else {
              return 'password is incorrect';
            }
         }  //test your login request in POSTMAN of the bookingapp.
      }); 
}



//no need encryption. 
module.exports.registerUser = (requestBody) => {
  //to add a new record for the users collection. 
  //we will encrypt the value of the password using the hashSync() of the brcypt library.

  // "salt" => this is the technical term used for the value that describe the algorithym that will run in order to encrypt the password.
  //10 is the default value of the salt. 
  //=> indicate the stregth of the encryption to the value. 
  //SYNTAX: library.hashSync(value, salt Rounds)
  let newUser = new User({
     firstName:  requestBody.firstName,
     middleName: requestBody.middleName,
     lastName: requestBody.lastName,
     email: requestBody.email,
     password: bcrypt.hashSync(requestBody.password, 10),
     mobileNo: requestBody.mobileNo,
     gender: requestBody.gender
  }); 

  //lets save the newly created user inside our database.
  //upon executing a save method to the constructed document. a promise-like state will be returned.
  return newUser.save().then((user, error) => {
  	//we will identify the proper response upon the resolve and rejected state of the promise.
  	//lets create a control structure to determine of the response of the 2 possible outcomes of the promise. 
  	if (error) {
  		//user registration has failed.
  		return false; 
  	} else {
  		return user; 
  	}
  })
}

//retrieve the information about the user. 
  //we need to be able to identify first who is the authenticated user.
module.exports.getProfile = (data) => {
   //we are going to execute a search query in our database to retrieve the details about the client. 
   return User.findById(data.userId).then(result => {
      //we will block the value of the password by reassigning a new value to it.
      //the important thing is to not show the hash/real value of the password.
      result.password = ""; //undefined or empty string. 

      //return the information back to the client.
      return result; 
   });
}